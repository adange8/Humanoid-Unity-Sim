using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragObjects : MonoBehaviour
{

    public Camera cam;
    public LayerMask draggableLayers;
    public float noObjectUnderMouseDefaultDistance = 10f;
    public MainCamControl cameraScript;
    public Transform humanoid;
    public Transform humanoidTorso;
    public float maxThrowVelocity;
    public float dragMomentum;
    public float aimAssistBias = 2f;
    public LogicManager logicManager;

    private GameObject objectToDrag;
    private Transform objectToDragParent;
    private Vector3 offsetToParent;
    private Vector3 objectToDragVelocity = Vector3.zero;

    public FeedbackPublisher jointPublisher;

    void Update()
    {
        bool IsMouseOverGameWindow = !(0 > Input.mousePosition.x || 0 > Input.mousePosition.y || Screen.width < Input.mousePosition.x || Screen.height < Input.mousePosition.y);
        if (!IsMouseOverGameWindow) {
            objectToDrag = null;
            if (objectToDragParent != null) {
                setKinematicRecursively(objectToDragParent, false);
                // setTriggerRecursively(objectToDragParent, false);
                if (objectToDragParent != humanoid) objectToDragVelocity = applyAimAssist(objectToDragVelocity, objectToDragParent, humanoidTorso);
                setVelocityRecursively(objectToDragParent, objectToDragVelocity);
            }
            objectToDragVelocity = Vector3.zero;
            objectToDragParent = null;
            cameraScript.canDrag = true;
            jointPublisher.areJointsValid = true;
            return;
        }
        if (Input.GetMouseButton(0) && objectToDrag == null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if ((draggableLayers == (draggableLayers | (1 << hit.collider.gameObject.layer)))) { // drag if mouse is over a draggable layer
                    objectToDrag = hit.collider.gameObject;
                    if (hit.collider.gameObject.layer == 3) {
                        objectToDragParent = humanoid;
                        logicManager.resetRobot();
                    } else {
                        objectToDragParent = hit.collider.transform;
                    }
                    setKinematicRecursively(objectToDragParent, true);
                    // setTriggerRecursively(objectToDragParent, true);
                    offsetToParent = objectToDragParent.position - getWorldPointUnderMouse();
                    cameraScript.canDrag = false;
                    jointPublisher.areJointsValid = false;
                }
            }
        }
        else if (!Input.GetMouseButton(0))
        {
            objectToDrag = null;
            if (objectToDragParent != null) {
                setKinematicRecursively(objectToDragParent, false);
                if (objectToDragParent != humanoid) objectToDragVelocity = applyAimAssist(objectToDragVelocity, objectToDragParent, humanoidTorso);
                setVelocityRecursively(objectToDragParent, objectToDragVelocity);
                // setTriggerRecursively(objectToDragParent, false);
            }
            objectToDragVelocity = Vector3.zero;
            objectToDragParent = null;
            cameraScript.canDrag = true;
            jointPublisher.areJointsValid = true;
        }
    }

    Vector3 applyAimAssist(Vector3 velocity, Transform obj, Transform target) {
        Vector3 directionToTarget = target.position - obj.position;
        directionToTarget.y = 0;
        directionToTarget.Normalize();
        
        float velocityMagnitude = velocity.magnitude;
        velocity.y = 0;
        velocity.Normalize();

        float dotProduct = Mathf.Max(0f, Vector3.Dot(directionToTarget, velocity)) * aimAssistBias;
        Vector3 aimAssistedVelocity = dotProduct * directionToTarget + (1.0f - dotProduct) * velocity;
        aimAssistedVelocity.Normalize();
        return aimAssistedVelocity * velocityMagnitude;
    }
    
    Vector3 getWorldPointUnderMouse()
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        // Create a layer mask that includes all layers except "Water" and "Air"
        int layerMask = ~((1 << LayerMask.NameToLayer("Humanoid")) | (1 << LayerMask.NameToLayer("Water")) | (1 << LayerMask.NameToLayer("Throwable")));

        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            return hit.point;
        }
        else
        {
            return ray.GetPoint(noObjectUnderMouseDefaultDistance);
        }
    }

    void setKinematicRecursively(Transform parentTransform, bool isKinematic)
    {
        // Disable kinematic for all articulation bodies in the current GameObject
        foreach (var articulationBody in parentTransform.GetComponents<ArticulationBody>())
        {
            articulationBody.enabled = !isKinematic;
        }

        foreach (var rigidbody in parentTransform.GetComponents<Rigidbody>())
        {
            rigidbody.isKinematic = isKinematic;
        }

        // Recursively call the function for each child
        for (int i = 0; i < parentTransform.childCount; i++)
        {
            setKinematicRecursively(parentTransform.GetChild(i), isKinematic);
        }
    }
    
    void setTriggerRecursively(Transform parentTransform, bool isTrigger)
    {
        // Disable kinematic for all articulation bodies in the current GameObject
        foreach (var collider in parentTransform.GetComponents<Collider>())
        {
            collider.isTrigger = isTrigger;
        }

        // Recursively call the function for each child
        for (int i = 0; i < parentTransform.childCount; i++)
        {
            setTriggerRecursively(parentTransform.GetChild(i), isTrigger);
        }
    }

    void setVelocityRecursively(Transform parentTransform, Vector3 velocity)
    {
        foreach (var rigidbody in parentTransform.GetComponents<Rigidbody>())
        {
            rigidbody.velocity = velocity;
            // rigidbody.AddForce(velocity, ForceMode.VelocityChange);
        }

        // Recursively call the function for each child
        for (int i = 0; i < parentTransform.childCount; i++)
        {
            setVelocityRecursively(parentTransform.GetChild(i), velocity);
        }
    }

    void FixedUpdate()
    {
        Update();
        if (objectToDrag != null) {
            // Calculate velocity manually
            Vector3 displacement = (getWorldPointUnderMouse() + offsetToParent) - objectToDragParent.position;
            objectToDragVelocity = objectToDragVelocity * dragMomentum + (displacement / Time.fixedDeltaTime);
            if (objectToDragVelocity.magnitude > maxThrowVelocity) objectToDragVelocity = maxThrowVelocity * (objectToDragVelocity / objectToDragVelocity.magnitude);
            
            objectToDragParent.position = getWorldPointUnderMouse() + offsetToParent;
        }
    }
}
