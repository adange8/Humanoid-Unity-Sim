using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Std;
using Unity.Robotics.ROSTCPConnector.ROSGeometry;

public class StatePublisher : MonoBehaviour
{
    private ArticulationBody ab;
    private Vector3 angularVelocity;
    private Vector3 linearAcceleration;
    private Vector3 globalVelocities;
    private Quaternion orientation;
    private float prevXVelocity = 0.0f;
    private float prevYVelocity = 0.0f;
    private float prevZVelocity = 0.0f;
    private ROSConnection roscon;
    public string unityStateTopic = "/unity/state";
    private RosMessageTypes.Humanoid.UnityStateMsg msg = new RosMessageTypes.Humanoid.UnityStateMsg();
    
    void Start()
    {
        ab = GetComponent<ArticulationBody>();
        roscon = ROSConnection.GetOrCreateInstance();
        roscon.RegisterPublisher<RosMessageTypes.Humanoid.UnityStateMsg>(unityStateTopic);
        
    }

    void FixedUpdate() {

        float currentVelocityX = ab.velocity.x;
        float currentVelocityY = ab.velocity.y;
        float currentVelocityZ = ab.velocity.z;
        float updateTime = Time.fixedDeltaTime;

        angularVelocity = ab.angularVelocity;
        msg.ang_vel = angularVelocity.To<RUF>();

        globalVelocities = new Vector3(currentVelocityX, currentVelocityY, currentVelocityZ);
        msg.global_vel = globalVelocities.To<RUF>();

        linearAcceleration = CalculateLinearAcceleration(currentVelocityX, currentVelocityY, currentVelocityZ, updateTime);
        msg.local_lin_accel = linearAcceleration.To<RUF>();
        
        orientation = transform.rotation; // * Quaternion.Euler(0f, -90f, 0f);
        msg.quat = orientation.To<NED>();

        prevXVelocity = ab.velocity.x;
        prevYVelocity = ab.velocity.y;
        prevZVelocity = ab.velocity.z;
        
        // publish to topics
        roscon.Publish(unityStateTopic, msg);

    }

    Vector3 CalculateLinearAcceleration(float currentVelocityX, float currentVelocityY, float currentVelocityZ, float updateTime)
    {   
        float xAccel = (currentVelocityX - prevXVelocity) / updateTime;
        float yAccel = (currentVelocityY - prevYVelocity) / updateTime;
        float zAccel = (currentVelocityZ - prevZVelocity) / updateTime;

        Vector3 linearAccel = new Vector3(xAccel, yAccel, zAccel);

        return linearAccel;
    }
}
