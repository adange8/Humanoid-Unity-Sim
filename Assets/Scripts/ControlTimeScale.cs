using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlTimeScale : MonoBehaviour
{
    private Slider timeScaleSlider;

    void Start()
    {
        timeScaleSlider = GetComponent<Slider>();
        timeScaleSlider.value = 1;
    }

    public void UpdateTimeScale()
    {
        Time.timeScale = timeScaleSlider.value;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }
}
