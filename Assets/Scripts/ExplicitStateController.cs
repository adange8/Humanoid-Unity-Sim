using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplicitStateController : MonoBehaviour
{
    public bool setAngularVelocity = false;
    public Vector3 angularVelocity = new Vector3(0, 0, 0);

    public bool setRotation = false;
    public Vector3 rotation = new Vector3(0, 0, 0);

    public bool setJointPositions = false;
    public List<float> jointPositions = new List<float>()
    {
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    };

    public bool setJointVelocities = false;
    public List<float> jointVelocities = new List<float>()
    {
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    };
    private List<float> jointVelocityPositionTracker = new List<float>()
    {
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0
    };

    public List<string> jointNames = new List<string>()
    {
        "right_shoulder_pitch",
        "right_shoulder_roll",
        "right_elbow",
        "left_shoulder_pitch",
        "left_shoulder_roll",
        "left_elbow",
        "left_hip_roll",
        "left_hip_pitch",
        "left_knee",
        "right_hip_roll",
        "right_hip_pitch",
        "right_knee"
    };


    private Dictionary<string, float> jointNameToJointPosition = new Dictionary<string, float>();
    private Dictionary<string, float> jointNameToJointVelocityPosition = new Dictionary<string, float>();

    private Dictionary<string, string> jointNameToArticulationBodyName = new Dictionary<string, string>()
        {
            {"right_shoulder_pitch", "right_shoulder_pitch_link"},
            {"right_shoulder_roll", "right_shoulder_roll_link"},
            {"right_elbow", "right_forearm_pitch_link"},
            {"left_shoulder_pitch", "left_shoulder_pitch_link"},
            {"left_shoulder_roll", "left_shoulder_roll_link"},
            {"left_elbow", "left_forearm_pitch_link"},
            {"left_hip_roll", "left_waist_roll_link"},
            {"left_hip_pitch", "left_waist_pitch_link"},
            {"left_knee", "left_knee_pitch_link"},
            {"right_hip_roll", "right_waist_roll_link"},
            {"right_hip_pitch", "right_waist_pitch_link"},
            {"right_knee", "right_knee_pitch_link"}
        };

    private void setRobotPositions()
    {
        for (int i = 0; i < jointNames.Count; i++)
        {
            jointNameToJointPosition[jointNames[i]] = 180f * jointPositions[i] / Mathf.PI;
        }
        foreach (KeyValuePair<string, float> jointNamePosition in jointNameToJointPosition)
        {
            MoveJoint(jointNameToArticulationBodyName[jointNamePosition.Key], jointNamePosition.Value);
        }
    }

    private void setRobotVelocities()
    {
        for (int i = 0; i < jointNames.Count; i++)
        {
            jointVelocityPositionTracker[i] += (180f * jointVelocities[i] / Mathf.PI) * Time.deltaTime;
            jointNameToJointVelocityPosition[jointNames[i]] = jointVelocityPositionTracker[i];
        }
        foreach (KeyValuePair<string, float> jointNamePosition in jointNameToJointVelocityPosition)
        {
            MoveJoint(jointNameToArticulationBodyName[jointNamePosition.Key], jointNamePosition.Value);
        }
    }

    private void MoveJoint(string name, float rotationValue)
    {
        ArticulationBody joint = FindArticulationBodyByName(name);
        ArticulationDrive currentDrive = joint.xDrive;
        currentDrive.target = rotationValue;
        joint.xDrive = currentDrive;
    }
   
    ArticulationBody FindArticulationBodyByName(string name)
    {
        GameObject jointGameObject = GameObject.Find(name);

        if (jointGameObject != null)
        {
            ArticulationBody articulationBody = jointGameObject.GetComponent<ArticulationBody>();

            if (articulationBody != null)
            {
                return articulationBody;
            }
            else
            {
                Debug.LogError("No ArticulationBody component found on GameObject with name: " + name);
            }
        }
        else
        {
            Debug.LogError("GameObject not found with name: " + name);
        }

        return null;
    }

    // Update is called once per frame
    void Update()
    {
        if (setAngularVelocity) setRotation = false;
        if (setRotation) setAngularVelocity = false;

        if (setJointPositions) setJointVelocities = false;
        if (setJointVelocities) setJointPositions = false;

        if (setJointPositions) setRobotPositions();
        if (setJointVelocities) setRobotVelocities();
        ArticulationBody torso = GetComponent<ArticulationBody>();
        if (setAngularVelocity) torso.angularVelocity = new Vector3(180f * angularVelocity.y / Mathf.PI, 180f * -angularVelocity.z / Mathf.PI, 180f * -angularVelocity.x / Mathf.PI);
        if (setRotation) transform.rotation = Quaternion.Euler(new Vector3(rotation.x, -rotation.z + 90, rotation.y));
    }
}
