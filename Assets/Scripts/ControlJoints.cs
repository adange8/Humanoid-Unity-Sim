using System.Collections.Generic;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using System.Linq;

public class ControlJoints : MonoBehaviour
{
    public float jointFriction = 10f;
    public float jointAngularDamping = 10f;
    public float maxForce = 150f;
    public string jointSetpointsTopic = "/servos/command";
    ROSConnection roscon;
    
    private Dictionary<string, string> jointNameToArticulationBodyName = new Dictionary<string, string>()
        {
            {"right_shoulder_pitch", "right_shoulder_pitch_link"},
            {"right_shoulder_roll", "right_shoulder_roll_link"},
            {"right_elbow", "right_forearm_pitch_link"},
            {"left_shoulder_pitch", "left_shoulder_pitch_link"},
            {"left_shoulder_roll", "left_shoulder_roll_link"},
            {"left_elbow", "left_forearm_pitch_link"},
            {"left_hip_roll", "left_waist_roll_link"},
            {"left_hip_pitch", "left_waist_pitch_link"},
            {"left_knee", "left_knee_pitch_link"},
            {"right_hip_roll", "right_waist_roll_link"},
            {"right_hip_pitch", "right_waist_pitch_link"},
            {"right_knee", "right_knee_pitch_link"}
        };
    
    private Dictionary<string, float> jointUpperLimits = new Dictionary<string, float>()
        {
            {"right_shoulder_pitch", 90f},
            {"right_shoulder_roll", 0f},
            {"right_elbow", 0f},
            {"left_shoulder_pitch", 90f},
            {"left_shoulder_roll", 90f},
            {"left_elbow", 0f},
            {"left_hip_roll", 90f},
            {"left_hip_pitch", 90f},
            {"left_knee", 90f},
            {"right_hip_roll", 0f},
            {"right_hip_pitch", 90f},
            {"right_knee", 90f}
        };
        
    private Dictionary<string, float> jointLowerLimits = new Dictionary<string, float>()
        {
            {"right_shoulder_pitch", -90f},
            {"right_shoulder_roll", -90f},
            {"right_elbow", -90f},
            {"left_shoulder_pitch", -90f},
            {"left_shoulder_roll", 0f},
            {"left_elbow", -90f},
            {"left_hip_roll", 0f},
            {"left_hip_pitch", -90f},
            {"left_knee", 0f},
            {"right_hip_roll", -90f},
            {"right_hip_pitch", -90f},
            {"right_knee", 0f}
        };

    void Start()
    {   
        Dictionary<string, string> articulationBodyNameToJointName = jointNameToArticulationBodyName.ToDictionary(x => x.Value, x => x.Key);

        ArticulationBody[] articulationChain = this.GetComponentsInChildren<ArticulationBody>();
        foreach (ArticulationBody articulation in articulationChain)
        {
            if (articulation.name == "torso") {
                continue;
            }
            articulation.jointType = ArticulationJointType.RevoluteJoint;
            articulation.jointFriction = jointFriction;
            articulation.angularDamping = jointAngularDamping;
            ArticulationDrive currentDrive = articulation.xDrive;
            string jointName = articulationBodyNameToJointName[articulation.name];
            currentDrive.lowerLimit = jointLowerLimits[jointName];
            currentDrive.upperLimit = jointUpperLimits[jointName];
            currentDrive.forceLimit = maxForce;
            currentDrive.driveType = ArticulationDriveType.Target;
            articulation.xDrive = currentDrive;
        }

        roscon = ROSConnection.GetOrCreateInstance();
        roscon.Subscribe<RosMessageTypes.Humanoid.ServoCommandMsg>(jointSetpointsTopic, JointSetpointsCb);
    }

    private void JointSetpointsCb(RosMessageTypes.Humanoid.ServoCommandMsg msg) {
        foreach (var field in msg.GetType().GetFields())
        {
            string attributeName = field.Name;
            string jointName = jointNameToArticulationBodyName.TryGetValue(attributeName, out string name) ? name : "";
            if (jointName != "") {
                float rotationValue = 180 * (float)field.GetValue(msg) / Mathf.PI;
                MoveJoint(jointName, rotationValue);
            }
        }
    }

    private void MoveJoint(string name, float rotationValue)
    {
        ArticulationBody joint = FindArticulationBodyByName(name);
        ArticulationDrive currentDrive = joint.xDrive;
        currentDrive.target = rotationValue;
        joint.xDrive = currentDrive;
    }
   
    ArticulationBody FindArticulationBodyByName(string name)
    {
        GameObject jointGameObject = GameObject.Find(name);

        if (jointGameObject != null)
        {
            ArticulationBody articulationBody = jointGameObject.GetComponent<ArticulationBody>();

            if (articulationBody != null)
            {
                return articulationBody;
            }
            else
            {
                Debug.LogError("No ArticulationBody component found on GameObject with name: " + name);
            }
        }
        else
        {
            Debug.LogError("GameObject not found with name: " + name);
        }

        return null;
    }
}