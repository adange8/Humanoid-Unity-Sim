using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Std;

public class VisualizeTorsoState : MonoBehaviour
{
    ROSConnection roscon;

    public string ZStateTopic;
    public string ThetaXStateTopic;
    public string ThetaYStateTopic;
    public string ThetaZStateTopic;
    public ArticulationBody torso;

    private Vector3 torsoPos;
    private Vector3 torsoEulerRotation;
    // Start is called before the first frame update
    void Start()
    {
        roscon = ROSConnection.GetOrCreateInstance();
        roscon.Subscribe<Float64Msg>(ZStateTopic, zStateCb);
        roscon.Subscribe<Float64Msg>(ThetaXStateTopic, zStateCb);
        roscon.Subscribe<Float64Msg>(ThetaYStateTopic, zStateCb);
        roscon.Subscribe<Float64Msg>(ThetaZStateTopic, zStateCb);
        torsoPos = torso.transform.position;
        torsoEulerRotation = torso.transform.rotation.eulerAngles;
    }

    // Update is called once per frame
    void zStateCb(Float64Msg msg)
    {
        torsoPos.z = (float)msg.data;
        torso.TeleportRoot(torsoPos, Quaternion.Euler(torsoEulerRotation));
    }
    
    // Update is called once per frame
    void thetaXStateCb(Float64Msg msg)
    {
        torsoEulerRotation.z = (float)-msg.data;
        torso.TeleportRoot(torsoPos, Quaternion.Euler(torsoEulerRotation));
    }
    
    // Update is called once per frame
    void thetaYStateCb(Float64Msg msg)
    {
        torsoEulerRotation.x = (float)msg.data;
        torso.TeleportRoot(torsoPos, Quaternion.Euler(torsoEulerRotation));
    }
    
    // Update is called once per frame
    void thetaZStateCb(Float64Msg msg)
    {
        torsoEulerRotation.y = (float)-msg.data;
        torso.TeleportRoot(torsoPos, Quaternion.Euler(torsoEulerRotation));
    }
}
