using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Std;
using Unity.Robotics.ROSTCPConnector.ROSGeometry;
using TMPro;

public class LogicManager : MonoBehaviour
{
    [Header("CAMERA CONTROL VARIABLES")]

    public GameObject headCam;
    public GameObject freeCam;
    public GameObject followCam;
    public GameObject depthCam;
    public Transform humanoidTorso;
    public float distanceToHumanoidWhenSnapping;

    private ROSConnection roscon;
    private List<ArticulationBody> articulationChain = new List<ArticulationBody>();
    private List<Vector3> articulationInitialPositions = new List<Vector3>();
    private List<Quaternion> articulationInitialRotations = new List<Quaternion>();

    // Start is called before the first frame update
    void Start()
    {
        getAllArticulationBodiesRecursively(humanoidTorso.parent, articulationChain);
        foreach (ArticulationBody ab in articulationChain)
        {
            articulationInitialPositions.Add(ab.transform.position);
            articulationInitialRotations.Add(ab.transform.rotation);
        }
        roscon = ROSConnection.GetOrCreateInstance();
        activateFollowCam();
        activateFreeCam();
        if (headCam == null || depthCam == null) {
            Debug.Log("Warning: Head Cam or Depth Cam attribute of GUILogic has not been defined.");
        }
    }

    public void activateHeadCam() {
        freeCam.SetActive(false);
        followCam.SetActive(false);
        if (headCam != null && depthCam != null) {
            depthCam.SetActive(false);
            headCam.SetActive(true);
        }
    }

    public void activateFreeCam() {
        freeCam.SetActive(true);
        followCam.SetActive(false);
        if (headCam != null && depthCam != null) {
            depthCam.SetActive(false);
            headCam.SetActive(false);
        }
    }
    
    public void activateFollowCam() {
        freeCam.SetActive(false);
        followCam.SetActive(true);
        if (headCam != null && depthCam != null) {
            depthCam.SetActive(false);
            headCam.SetActive(false);
        }
    }

    public void activateDepthCam() {
        freeCam.SetActive(false);
        followCam.SetActive(false);
        if (headCam != null && depthCam != null) {
            depthCam.SetActive(true);
            headCam.SetActive(false);
        }
    }

    public void snapFreeCam() {
        if (freeCam.activeSelf) {
            freeCam.transform.LookAt(humanoidTorso);
            Vector3 directionFromTarget = freeCam.transform.position - humanoidTorso.position;
            freeCam.transform.position = humanoidTorso.position + directionFromTarget.normalized * distanceToHumanoidWhenSnapping;
        }
    }
    
    public void reloadScene()
    {
        Scene currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.name);
    }

    public void loadScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    void zeroRobotJoints(ArticulationBody ab)
    {
        List<float> jointPositions = new List<float>();
        for (int i = 0; i < jointPositions.Count; i++)
        {
            jointPositions[i] = 0f;
        }
        ab.GetJointPositions(jointPositions);
        ab.SetJointPositions(jointPositions);
        ab.SetDriveTargets(jointPositions);
    }

    public void resetRobot()
    {
        Vector3 offsetFromInitial = humanoidTorso.position - articulationInitialPositions[articulationChain.IndexOf(humanoidTorso.GetComponent<ArticulationBody>())];
        float groundLevel = -1000.0f;
        foreach (ArticulationBody ab in articulationChain)
        {
            groundLevel = Mathf.Max(groundLevel, ab.transform.position.y);
        }
        
        foreach (ArticulationBody ab in articulationChain)
        {
            ab.gameObject.SetActive(false);
            zeroRobotJoints(ab);
            ab.transform.position = new Vector3(articulationInitialPositions[articulationChain.IndexOf(ab)].x + offsetFromInitial.x, articulationInitialPositions[articulationChain.IndexOf(ab)].y + groundLevel, articulationInitialPositions[articulationChain.IndexOf(ab)].z + offsetFromInitial.z);
            ab.transform.rotation = articulationInitialRotations[articulationChain.IndexOf(ab)];
            ab.gameObject.SetActive(true);
        }
    }

    void getAllArticulationBodiesRecursively(Transform parentTransform, List<ArticulationBody> articulationChain)
    {
        foreach (Transform child in parentTransform)
        {
            ArticulationBody ab = child.GetComponent<ArticulationBody>();
            if (ab != null)
            {
                articulationChain.Add(ab);
            }
            getAllArticulationBodiesRecursively(child, articulationChain);
        }
    }

}
