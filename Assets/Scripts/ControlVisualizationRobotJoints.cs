using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using System.Reflection;
using System.Linq;
using UnityEngine.UI;

public class ControlVisualizationRobotJoints : MonoBehaviour
{
    ROSConnection roscon;
    public Camera cam;
    public LayerMask draggableLayers;
    public MainCamControl cameraScript;
    public float dragForce = 100f;
    public float fineControlTorque = 1000000f;
    public GameObject humanoid;
    public ArticulationBody humanoidRootArticulationBody;
    public GameObject dragIndicator;
    public string jointCommandsTopic = "/servos/command";

    private ArticulationBody objectToDrag;
    private Vector3 initialMousePosition;
    private Vector3 initialCamDirection;
    private Vector3 velocityRef;
    
    public UpdateJointControlGUI jointControlGUIScript;
    
    private Dictionary<string, string> jointNameToArticulationBodyName = new Dictionary<string, string>()
    {
        {"right_shoulder_pitch", "right_shoulder_pitch_link"},
        {"right_shoulder_roll", "right_shoulder_roll_link"},
        {"right_elbow", "right_forearm_pitch_link"},
        {"left_shoulder_pitch", "left_shoulder_pitch_link"},
        {"left_shoulder_roll", "left_shoulder_roll_link"},
        {"left_elbow", "left_forearm_pitch_link"},
        {"left_hip_roll", "left_waist_roll_link"},
        {"left_hip_pitch", "left_waist_pitch_link"},
        {"left_knee", "left_knee_pitch_link"},
        {"right_hip_roll", "right_waist_roll_link"},
        {"right_hip_pitch", "right_waist_pitch_link"},
        {"right_knee", "right_knee_pitch_link"}
    };

    Dictionary<string, string> articulationBodyNameToJointName;

    void Start() {
        articulationBodyNameToJointName = jointNameToArticulationBodyName.ToDictionary(x => x.Value, x => x.Key);
        roscon = ROSConnection.GetOrCreateInstance();
        roscon.RegisterPublisher<RosMessageTypes.Humanoid.ServoCommandMsg>(jointCommandsTopic);
    }

    void FixedUpdate()
    {
        if (Input.GetMouseButton(0) && objectToDrag == null)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if ((draggableLayers == (draggableLayers | (1 << hit.collider.gameObject.layer)))) { // drag if mouse is over a draggable layer
                    objectToDrag = FindComponentInParents<ArticulationBody>(hit.collider.transform);
                    cameraScript.canDrag = false;
                    initialMousePosition = hit.point;
                    dragIndicator.transform.position = hit.point;
                    dragIndicator.transform.parent = hit.collider.transform;
                    initialCamDirection = cam.transform.forward;
                    MakeRobotMovable();
                }
            }
        }
        else if (!Input.GetMouseButton(0) && objectToDrag != null)
        {
            objectToDrag = null;
            dragIndicator.transform.position = new Vector3(10000, 10000, 10000);
            dragIndicator.transform.parent = null;
            cameraScript.canDrag = true;
            MakeRobotFrozen();
        }

        if (objectToDrag != null) {
            SetRobotTargetsToCurrentPositions();
            dragJoint();
        }
    }

    private void dragJoint() {
        Vector3 mouseDirectionFromCam = cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f)) - cam.transform.position;
        Vector3 newFocusedPoint = calculateIntersection(cam.transform.position, mouseDirectionFromCam, initialMousePosition, initialCamDirection);
        Vector3 forceDirection = newFocusedPoint - dragIndicator.transform.position;
        Vector3 force = forceDirection * Time.fixedDeltaTime * dragForce;
        objectToDrag.AddForceAtPosition(force, dragIndicator.transform.position, ForceMode.Force);
    }

    public void ResetJointsToZero() {
        List<float> jointPositions = new List<float>();
        humanoidRootArticulationBody.GetJointPositions(jointPositions);
        for (int i = 0; i < jointPositions.Count; i++)
        {
            jointPositions[i] = 0f;
        }
        humanoidRootArticulationBody.SetDriveTargets(jointPositions);

        RosMessageTypes.Humanoid.ServoCommandMsg jointSetpoints = new RosMessageTypes.Humanoid.ServoCommandMsg();
        ArticulationBody[] articulationChain = humanoid.GetComponentsInChildren<ArticulationBody>();
        foreach (ArticulationBody joint in articulationChain)
        {
            string jointName = articulationBodyNameToJointName.TryGetValue(joint.gameObject.name, out string name) ? name : "";
            if (jointName == "") continue;
            
            FieldInfo field = typeof(RosMessageTypes.Humanoid.ServoCommandMsg).GetField(jointName);
            if (field != null)
            {
                field.SetValue(jointSetpoints, 0f);
            }
            else
            {
                Debug.LogError("DRAGROBOTJOINTS: Attribute " + jointName + " not found (related to articulationBody " + joint.gameObject.name + ").");
            }
        }
        roscon.Publish(jointCommandsTopic, jointSetpoints);
    }

    public void SetRobotTargetsToCurrentPositions() {
        List<float> jointPositions = new List<float>();
        humanoidRootArticulationBody.GetJointPositions(jointPositions);
        humanoidRootArticulationBody.SetDriveTargets(jointPositions);

        RosMessageTypes.Humanoid.ServoCommandMsg jointSetpoints = new RosMessageTypes.Humanoid.ServoCommandMsg();
        ArticulationBody[] articulationChain = humanoid.GetComponentsInChildren<ArticulationBody>();
        foreach (ArticulationBody joint in articulationChain)
        {
            string jointName = articulationBodyNameToJointName.TryGetValue(joint.gameObject.name, out string name) ? name : "";
            if (jointName == "") continue;
            
            FieldInfo field = typeof(RosMessageTypes.Humanoid.ServoCommandMsg).GetField(jointName);
            if (field != null)
            {
                field.SetValue(jointSetpoints, joint.jointPosition[0]);
            }
            else
            {
                Debug.LogError("DRAGROBOTJOINTS: Attribute " + jointName + " not found (related to articulationBody " + joint.gameObject.name + ").");
            }
        }
        roscon.Publish(jointCommandsTopic, jointSetpoints);
    }

    public void MakeRobotMovable() {
        ArticulationBody[] articulationChain = humanoid.GetComponentsInChildren<ArticulationBody>();
        foreach (ArticulationBody joint in articulationChain)
        {
            joint.jointFriction = 0;
            joint.angularDamping = Mathf.Infinity;
            ArticulationDrive currentDrive = joint.xDrive;
            currentDrive.forceLimit = 0;
            joint.xDrive = currentDrive;
        }
    }
    
    public void MakeRobotFrozen() {
        ArticulationBody[] articulationChain = humanoid.GetComponentsInChildren<ArticulationBody>();
        foreach (ArticulationBody joint in articulationChain)
        {
            joint.jointFriction = 0;
            joint.angularDamping = 0;
            ArticulationDrive currentDrive = joint.xDrive;
            currentDrive.forceLimit = 50000;
            joint.xDrive = currentDrive;
        }
    }

    Vector3 calculateIntersection(Vector3 origin, Vector3 direction, Vector3 planePoint, Vector3 planeNormal)
    {
        // Calculate the distance from the plane to the origin of the vector
        float distanceToPlane = Vector3.Dot(planeNormal, planePoint - origin) / Vector3.Dot(planeNormal, direction);
        // Calculate the intersection point
        Vector3 intersectionPoint = origin + direction * distanceToPlane;

        return intersectionPoint;
    }
    
    T FindComponentInParents<T>(Transform child) where T : Component
    {
        // Traverse the hierarchy upwards until reaching the root
        while (child != null)
        {
            // Check if the component is attached to the current GameObject
            T component = child.GetComponent<T>();

            // If found, return the component
            if (component != null)
            {
                return component;
            }

            // Move up to the parent GameObject
            child = child.parent;
        }

        // Component not found in parents
        return null;
    }

    public void UpdateJointSetpointsFromSliders() {
        
        RosMessageTypes.Humanoid.ServoCommandMsg jointSetpoints = new RosMessageTypes.Humanoid.ServoCommandMsg();
        
        foreach (KeyValuePair<string, Slider> pair in jointControlGUIScript.jointSetpointSlider)
        {
            string jointName = pair.Key;
            
            FieldInfo field = typeof(RosMessageTypes.Humanoid.ServoCommandMsg).GetField(jointName);
            if (field != null)
            {
                field.SetValue(jointSetpoints, pair.Value.value);
            }
            else
            {
                Debug.LogError("DRAGROBOTJOINTS: Attribute " + jointName + " not found.");
            }
        }
        roscon.Publish(jointCommandsTopic, jointSetpoints);
    }
}
