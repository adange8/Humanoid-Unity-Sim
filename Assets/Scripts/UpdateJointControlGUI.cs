using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Robotics.ROSTCPConnector;
using System.Reflection;

public class UpdateJointControlGUI : MonoBehaviour
{
    
    ROSConnection roscon;
    public string jointCommandsTopic = "/servos/command";
    public Slider right_shoulder_pitch_slider;
    public Slider right_shoulder_roll_slider;
    public Slider right_forearm_pitch_slider;
    public Slider left_shoulder_pitch_slider;
    public Slider left_shoulder_roll_slider;
    public Slider left_forearm_pitch_slider;
    public Slider left_waist_roll_slider;
    public Slider left_waist_pitch_slider;
    public Slider left_knee_pitch_slider;
    public Slider right_waist_roll_slider;
    public Slider right_waist_pitch_slider;
    public Slider right_knee_pitch_slider;

    [HideInInspector]
    public Dictionary<string, Slider> jointSetpointSlider;

    void Start() {
        jointSetpointSlider = new Dictionary<string, Slider>()
        {
            {"right_shoulder_pitch", right_shoulder_pitch_slider},
            {"right_shoulder_roll", right_shoulder_roll_slider},
            {"right_elbow", right_forearm_pitch_slider},
            {"left_shoulder_pitch", left_shoulder_pitch_slider},
            {"left_shoulder_roll", left_shoulder_roll_slider},
            {"left_elbow", left_forearm_pitch_slider},
            {"left_hip_roll", left_waist_roll_slider},
            {"left_hip_pitch", left_waist_pitch_slider},
            {"left_knee", left_knee_pitch_slider},
            {"right_hip_roll", right_waist_roll_slider},
            {"right_hip_pitch", right_waist_pitch_slider},
            {"right_knee", right_knee_pitch_slider}
        };
        
        roscon = ROSConnection.GetOrCreateInstance();
        roscon.Subscribe<RosMessageTypes.Humanoid.ServoCommandMsg>(jointCommandsTopic, jointSetpointCb);
    }

    void jointSetpointCb(RosMessageTypes.Humanoid.ServoCommandMsg msg) {
        foreach (var field in msg.GetType().GetFields())
        {
            string attributeName = field.Name;
            Slider jointSlider = jointSetpointSlider.TryGetValue(attributeName, out Slider s) ? s : null;
            if (jointSlider != null) {
                float setpoint = (float) field.GetValue(msg);
                jointSlider.SetValueWithoutNotify(setpoint);
            }
        }
    }
}
