using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector;

public class VisualizeJointStates : MonoBehaviour
{
    ROSConnection roscon;

    public string jointStatesTopic = "/servos/feedback";
    public ArticulationBody torso;

    private Dictionary<string, string> jointNameToArticulationBodyName = new Dictionary<string, string>()
    {
        {"right_shoulder_pitch", "right_shoulder_pitch_link"},
        {"right_shoulder_roll", "right_shoulder_roll_link"},
        {"right_elbow", "right_forearm_pitch_link"},
        {"left_shoulder_pitch", "left_shoulder_pitch_link"},
        {"left_shoulder_roll", "left_shoulder_roll_link"},
        {"left_elbow", "left_forearm_pitch_link"},
        {"left_hip_roll", "left_waist_roll_link"},
        {"left_hip_pitch", "left_waist_pitch_link"},
        {"left_knee", "left_knee_pitch_link"},
        {"right_hip_roll", "right_waist_roll_link"},
        {"right_hip_pitch", "right_waist_pitch_link"},
        {"right_knee", "right_knee_pitch_link"}
    };

    // Start is called before the first frame update
    void Start()
    {
        ArticulationBody[] articulationChain = this.GetComponentsInChildren<ArticulationBody>();
        foreach (ArticulationBody joint in articulationChain)
        {
            joint.inertiaTensor = new Vector3(0.00001f,0.00001f,0.00001f);
            joint.inertiaTensorRotation = Quaternion.Euler(0f,90f,0f);
            joint.jointType = ArticulationJointType.RevoluteJoint;
            joint.jointFriction = 0;
            joint.angularDamping = 0;
            ArticulationDrive currentDrive = joint.xDrive;
            currentDrive.forceLimit = 50000;
            currentDrive.upperLimit = 180f;
            currentDrive.lowerLimit = -180f;
            currentDrive.driveType = ArticulationDriveType.Target;
            joint.xDrive = currentDrive;
        }

        torso.immovable = true;

        Collider[] colliderChain = this.GetComponentsInChildren<Collider>();
        foreach (Collider coll in colliderChain)
        {
            coll.isTrigger = true;
        }

        roscon = ROSConnection.GetOrCreateInstance();
        roscon.Subscribe<RosMessageTypes.Humanoid.ServoFeedbackMsg>(jointStatesTopic, JointStateCb);
    }

    private void JointStateCb(RosMessageTypes.Humanoid.ServoFeedbackMsg msg) {
        foreach (var field in msg.GetType().GetFields())
        {
            // Get the name of the attribute
            string attributeName = field.Name;
            string jointName = jointNameToArticulationBodyName.TryGetValue(attributeName, out string name) ? name : "";
            if (jointName != "") {
                float[] servoFeedback = (float[]) field.GetValue(msg);
                if (servoFeedback.Length > 0) {
                    float rotationValue = 180 * servoFeedback[0] / Mathf.PI;
                    MoveJoint(jointName, rotationValue);
                } else {
                    Debug.Log("WARN: Missing values in servo feedback!");
                }
            }
        }
    }

    private void MoveJoint(string name, float rotationValue)
    {
        ArticulationBody joint = FindArticulationBodyByName(name);
        ArticulationDrive currentDrive = joint.xDrive;
        currentDrive.target = rotationValue;
        joint.xDrive = currentDrive;
    }

    ArticulationBody FindArticulationBodyByName(string name)
    {
        GameObject jointGameObject = GameObject.Find(name);

        if (jointGameObject != null)
        {
            ArticulationBody articulationBody = jointGameObject.GetComponent<ArticulationBody>();

            if (articulationBody != null)
            {
                return articulationBody;
            }
            else
            {
                Debug.LogError("No ArticulationBody component found on GameObject with name: " + name);
            }
        }
        else
        {
            Debug.LogError("GameObject not found with name: " + name);
        }

        return null;
    }
}
