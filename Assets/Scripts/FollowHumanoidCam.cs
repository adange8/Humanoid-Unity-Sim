using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowHumanoidCam : MonoBehaviour
{
    public Transform humanoid;
    public Camera cam;
    public float distanceToFollowHumanoid = 3f;

    // Update is called once per frame
    void Update()
    {
        cam.transform.LookAt(humanoid);
        Vector3 directionFromTarget = cam.transform.position - humanoid.position;
        cam.transform.position = humanoid.position + directionFromTarget.normalized * distanceToFollowHumanoid;
    }
}
