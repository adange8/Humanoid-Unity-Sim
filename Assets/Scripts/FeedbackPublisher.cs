using System.Collections.Generic;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector;


public class FeedbackPublisher : MonoBehaviour {

    ROSConnection roscon;
    [HideInInspector]
    public bool areJointsValid = true;
    private Dictionary<string, string> articulationBodyNameToJointFbName = new Dictionary<string, string>()
    {
        {"right_shoulder_pitch_link", "right_shoulder_pitch"},
        {"right_shoulder_roll_link", "right_shoulder_roll"},
        {"right_forearm_pitch_link", "right_elbow"},
        {"left_shoulder_pitch_link", "left_shoulder_pitch"},
        {"left_shoulder_roll_link", "left_shoulder_roll"},
        {"left_forearm_pitch_link", "left_elbow"},
        {"left_waist_roll_link", "left_hip_roll"},
        {"left_waist_pitch_link", "left_hip_pitch"},
        {"left_knee_pitch_link", "left_knee"},
        {"right_waist_roll_link", "right_hip_roll"},
        {"right_waist_pitch_link", "right_hip_pitch"},
        {"right_knee_pitch_link", "right_knee"}
    };
    
    public string jointFeedbackTopic = "/servos/feedback";

    private ArticulationBody[] articulationChain;

    public float timePublish = 0.005f;

    void Start()
    {
        roscon = ROSConnection.GetOrCreateInstance();
        roscon.RegisterPublisher<RosMessageTypes.Humanoid.ServoFeedbackMsg>(jointFeedbackTopic);

        articulationChain = this.GetComponentsInChildren<ArticulationBody>();

        InvokeRepeating("UpdateFunction", 0f, timePublish);
    }

    void UpdateFunction()
    {   
        if (!areJointsValid) return;
        RosMessageTypes.Humanoid.ServoFeedbackMsg msg = new RosMessageTypes.Humanoid.ServoFeedbackMsg();

        foreach (ArticulationBody articulation in articulationChain)
        {
            string jointFbName = articulationBodyNameToJointFbName.TryGetValue(articulation.gameObject.name, out string name) ? name : "";

            if (jointFbName == "") continue;
            
            float pos = articulation.jointPosition[0];
            float vel = articulation.jointVelocity[0];
            float load = articulation.jointForce[0];

            if (!string.IsNullOrEmpty(jointFbName)){
                var field = msg.GetType().GetField(jointFbName);
                if (field != null){
                    field.SetValue(msg, new float[] {pos, vel, load});
                }
            }
        }

        roscon.Publish(jointFeedbackTopic, msg);
    }

}