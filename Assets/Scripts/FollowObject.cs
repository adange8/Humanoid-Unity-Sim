using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowRobotHead : MonoBehaviour
{

    public Transform humanoidHead;
    public Vector3 rotationOffset;
    public Vector3 positionOffset;

    // Update is called once per frame
    void Update()
    {
        transform.rotation = humanoidHead.rotation * Quaternion.Euler(rotationOffset.x, rotationOffset.y, rotationOffset.z);
        transform.position = humanoidHead.position;
        transform.localPosition = transform.localPosition + positionOffset;
    }
}
